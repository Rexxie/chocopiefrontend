///<reference path="../lib/core.d.ts"/>
///<reference path="AppFacade.ts"/>
///<reference path="Mappers/UserMapper.ts"/>

module Application{
    export class Application{
        public static Start(): void{
            var page = new AppPage();
            var navigation = page.GetNavigationView();
            var facade = new AppFacade(page);
            var router = Router.GetInstance();

            new UserMapper().GetCurrentUser((user: User) => {
                if (user.userType == 'Employer') {

                    router.SetModule('employer', 'adverts', () => {
                        facade.EmployerShowAdverts();
                    });
                    router.SetModule('employer', 'advertInfo', (args) => {
                        facade.EmployerAdvertInfo(args.id);
                    });
                    router.SetModule('employer', 'createAdvert', () => {
                        facade.EmployerCreateAdvert();
                    });

                    router.SetModule('employer', 'init', () => {
                        facade.EmployerInit(() => {
                            router.Go('employer', 'adverts');
                        });
                    });

                    navigation.AddItem('Adverts', () => {
                        router.Go('employer', 'adverts');
                    });
                    navigation.AddItem('Create Advert', () => {
                        router.Go('employer', 'createAdvert');
                    });

                    return router.Go('employer', 'init')
                }

                if (user.userType == 'Worker') {
                    router.SetModule('worker', 'adverts', () => {
                        facade.WorkerAdverts();
                    });
                    router.SetModule('worker', 'edit', () => {
                        facade.WorkerEdit();
                    });

                    router.SetModule('worker', 'init', () => {
                        facade.WorkerInit(() => {
                            router.Go('worker', 'adverts');
                        });
                    });

                    navigation.AddItem('Adverts', () => {
                        router.Go('worker', 'adverts');
                    });

                    navigation.AddItem('Edit', () => {
                        router.Go('worker', 'edit');
                    });

                    return router.Go('worker', 'init');
                }

                if (user.userType == 'admin') {
                    return router.Go('admin', 'init');
                }
            });
        }
    }
}