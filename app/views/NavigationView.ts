///<reference path="../../lib/core.d.ts"/>

module Application{
    export class NavigationView{
        private container:Component;
        constructor(body: Body){
            var nav = new Component('nav');
            nav.SetAttributes('class', 'navbar navbar-inverse');

            var nav_container = new Component('div');
            nav_container.SetAttributes('class', 'container-fluid');

            this.container = new Component('ul');
            this.container.SetAttributes('class', 'nav navbar-nav');

            nav_container.Append(this.container);
            nav.Append(nav_container);

            body.Append(nav);
        }

        public AddItem(title: string, handler:() => void): void{
            var item = new Component('li');
            var a = new Component('a');
            a.SetContent(title);
            a.On('click', () => {
                handler();
            });
            item.Append(a);
            this.container.Append(item);
        }
    }
}
