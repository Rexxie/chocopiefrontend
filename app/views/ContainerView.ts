///<reference path="../../lib/core.d.ts"/>

module Application{
    export class ContainerView {
        private container:Component;
        private notification: Component;

        constructor(body: Body) {
            this.container = new Component('div');
            this.container.SetAttributes('class', 'row');
            this.notification = Notification.GetInstance().GetNotification();

            body.Append(this.notification);
            body.Append(this.container);
        }

        public Append(component:Component):void {
            this.container.Append(component);
        }

        public Clear():void{
            this.container.Clear();
        }

    }
}
