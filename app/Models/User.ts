module Application {
    export class User {
        public login: string;
        public userType: string;
        constructor(args: {login: string; userType: string}){
            this.login = args.login;
            this.userType = args.userType;
        }
    }
}