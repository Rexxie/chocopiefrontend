///<reference path="../Models/User.ts"/>
///<reference path="../../lib/core.d.ts"/>

module Application{
    export class UserMapper{

        public GetCurrentUser(handler:(user: User) => void): void {
            new HttpClient().Execute({
                url: 'data/user.json',
                method: 'GET',
                callback: (data) => {
                    handler(this.BuildModel(data));
                }
            });
        }

        private BuildModel(jsonData): User{
            if(jsonData) {
                var data = jsonData.data;
                return new User({
                    login: data.login,
                    userType: data.userType
                });
            }

            return null;
        }
    }
}
