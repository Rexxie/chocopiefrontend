///<reference path="../lib/core.d.ts"/>
///<reference path="views/ContainerView.ts"/>
///<reference path="views/NavigationView.ts"/>

module Application {
    export class AppPage {
        container: ContainerView;
        navigation: NavigationView;
        constructor() {
            this.navigation = new NavigationView(Body.GetInstance());
            this.container = new ContainerView(Body.GetInstance());
        }

        public Append(element: Component):void {
            this.container.Append(element);
        }
        public Clear(): void{
            this.container.Clear();
        }
        public GetNavigationView(): NavigationView{
            return this.navigation;
        }
    }
}