///<reference path="AppPage.ts"/>
///<reference path="../modules/modulesDeclaration/employer.d.ts"/>
///<reference path="../modules/modulesDeclaration/worker.d.ts"/>
///<reference path="../lib/core.d.ts"/>

module Application{
    export class AppFacade{
        private page: AppPage;
        private loadedModules: Collection<string> = new Collection<string>();

        constructor(page: AppPage) {
            this.page = page;
        }
        public EmployerInit(handler:() => void): void{
            if(!this.loadedModules.Has('employer')) {
                new ScriptLoader('build/employer.js').Load(() => {
                    this.loadedModules.Add('employer');
                    handler();
                });
            }
        }
        public EmployerShowAdverts(): void {
            this.page.Clear();

            var empLayout = new Component('div');
            empLayout.SetAttributes('class', 'col-sm-6 col-centered');
            this.page.Append(empLayout);

            if (this.loadedModules.Has('employer')) {
                new Employer.EmployerFacade().ShowAdverts(empLayout);
            }
        }
        public EmployerAdvertInfo(id: number): void{
            this.page.Clear();

            var empLayout = new Component('div');
            empLayout.SetAttributes('class', 'col-sm-4');
            this.page.Append(empLayout);

            if (this.loadedModules.Has('employer')) {
                new Employer.EmployerFacade().ShowAdvertInfo(empLayout, id);
            }
        }
        public EmployerCreateAdvert(): void{
            this.page.Clear();

            var empLayout = new Component('div');
            empLayout.SetAttributes('class', 'col-sm-6 col-centered');
            this.page.Append(empLayout);

            if (this.loadedModules.Has('employer')) {
                new Employer.EmployerFacade().CreateAdvert(empLayout);
            }
        }
        public WorkerInit(handler:() => void): void{
            if(!this.loadedModules.Has('worker')) {
                new ScriptLoader('build/worker.js').Load(() => {
                    this.loadedModules.Add('worker');
                    handler();
                });
            }
        }
        public WorkerAdverts(): void{
            this.page.Clear();

            var workerLayout = new Component('div');
            workerLayout.SetAttributes('class', 'col-sm-6 col-centered');
            this.page.Append(workerLayout);

            if (this.loadedModules.Has('worker')) {
                new WorkerModule.WorkerFacade().ShowAdverts(workerLayout);
            }
        }
        public WorkerEdit(): void{
            this.page.Clear();

            var workerLayout = new Component('div');
            workerLayout.SetAttributes('class', 'col-sm-6 col-centered');
            this.page.Append(workerLayout);

            if (this.loadedModules.Has('worker')) {
                new WorkerModule.WorkerFacade().ShowEdit(workerLayout);
            }
        }
    }
}