var gulp = require('gulp');
var typescript = require('gulp-typescript');
//var glob = require('glob');
var rename = require('gulp-rename');

gulp.task('default', function () {
    gulp.start([
        'core',
        'app',
        'employer',
        'worker'
    ])
});

gulp.task('core', function () {
    gulp.task('coreTask', function () {
        var g = gulp.src('./core/core.reference.ts')
            .pipe(typescript({
                out: './public/build/core.js',
                target: 'es5',
                removeComments: true,
                declaration: true
            }));

        g.dts.pipe(rename('core.d.ts'));
        g.dts.pipe(gulp.dest('lib'));

        g.pipe(gulp.dest('./'));
    });
    gulp.start('coreTask');
    gulp.watch('./core/**/*.ts', function () {
        gulp.start('coreTask');
    });
});
gulp.task('app', function () {
    gulp.task('appTask', function () {
        var g = gulp.src('./app/app.reference.ts')
            .pipe(typescript({
                out: './public/build/app.js',
                target: 'es5',
                removeComments: true
            }));

        g.pipe(gulp.dest('./'));
    });
    gulp.start('appTask');
    gulp.watch('./app/**/*.ts', function () {
        gulp.start('appTask');
    });
});

gulp.task('employer', function () {
    gulp.task('employerTask', function () {
        var g = gulp.src('./modules/employer/employer.reference.ts')
            .pipe(typescript({
                out: './public/build/employer.js',
                target: 'es5',
                removeComments: true,
                declaration: true
            }));

        g.dts.pipe(rename('employer.d.ts'));
        g.dts.pipe(gulp.dest('modules/modulesDeclaration'));

        g.pipe(gulp.dest('./'));
        g.pipe(gulp.dest('./'));
    });
    gulp.start('employerTask');
    gulp.watch('./modules/employer/**/*.ts', function () {
        gulp.start('employerTask');
    });
});

gulp.task('worker', function () {
    gulp.task('workerTask', function () {
        var g = gulp.src('./modules/worker/worker.reference.ts')
            .pipe(typescript({
                out: './public/build/worker.js',
                target: 'es5',
                removeComments: true,
                declaration: true
            }));

        g.dts.pipe(rename('worker.d.ts'));
        g.dts.pipe(gulp.dest('modules/modulesDeclaration'));

        g.pipe(gulp.dest('./'));
        g.pipe(gulp.dest('./'));
    });
    gulp.start('workerTask');
    gulp.watch('./modules/worker/**/*.ts', function () {
        gulp.start('workerTask');
    });
});