///<reference path="../mappers/AdvertsMapper.ts"/>
module Employer{
    export class AdvertsService{
        constructor(){

        }
        public LoadAdverts(handler:(adverts)=>void): void {
            new AdvertsMapper().Load((adverts) => {
                handler(adverts);
            })
        }
        public RemoveAdvert(advertId: number, deletedHandler: () => void): void{
            deletedHandler();
        }
    }
}