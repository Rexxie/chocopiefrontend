///<reference path="../mappers/AdvertsMapper.ts"/>
///<reference path="../models/Advert.ts"/>
///<reference path="../mappers/RecommendationMapper.ts"/>

module Employer{
    export class AdvertInfoService{
        private advertId: number;
        constructor(advertId: number){
            this.advertId = advertId;
        }
        public GetAdvert(handler:(advert: Advert) => void): void{
            new AdvertsMapper().Get(this.advertId, (advert) => {
                handler(advert);
            })
        }
        public GetAdvertRecommendation(handler:(recommendations) => void): void{
            new RecommendationMapper().Load(this.advertId, (recommendations) => {
                handler(recommendations);
            });
        }
        public ApplyWorker(workerId: number, advertId: number): void{
            /**
             * send request to create contract
             */
        }
    }
}