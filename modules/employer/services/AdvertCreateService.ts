///<reference path="../mappers/ProfessionMapper.ts"/>
///<reference path="../mappers/SkillMapper.ts"/>
module Employer{
    export class AdvertCreateService{
        public CreateAdvert(data, createdHandler:() => void): void{
            console.log("AdvertData", data);
            createdHandler();
        }
        public LoadProfessions(handler:(professions) => void): void{
            new ProfessionMapper().Load((professions) => {
                handler(professions);
            });
        }
        public LoadSkills(handler:(skills) => void): void{
            new SkillMapper().Load((skills) => {
                handler(skills);
            });
        }
    }
}
