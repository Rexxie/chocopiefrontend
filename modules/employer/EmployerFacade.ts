///<reference path="../../lib/core.d.ts"/>
///<reference path="views/AdvertsView.ts"/>
///<reference path="presenter/AdvertsPresenter.ts"/>
///<reference path="views/AdvertInfoView.ts"/>
///<reference path="services/AdvertInfoService.ts"/>
///<reference path="presenter/AdvertInfoPresenter.ts"/>
///<reference path="views/AdvertCreateView.ts"/>
///<reference path="services/AdvertCreateService.ts"/>
///<reference path="presenter/AdvertCreatePresenter.ts"/>

module Employer{
    export class EmployerFacade{
        constructor(){

        }
        public ShowAdverts(layout: Component): void{
            var view = new AdvertsView(layout);
            var service = new AdvertsService();
            new AdvertsPresenter(view, service).ShowAdvertsAction();
        }

        /**
         * @param layout
         * @param id
         * @constructor
         */
        public ShowAdvertInfo(layout: Component, id: number): void{
            var view = new AdvertInfoView(layout);
            var service = new AdvertInfoService(id);

            new AdvertInfoPresenter(view, service).LoadAction();
        }

        /**
         *
         * @param layout
         * @constructor
         */
        public CreateAdvert(layout: Component): void{
            var view = new AdvertCreateView(layout);
            var service = new AdvertCreateService();
            var presenter = new AdvertCreatePresenter(view, service);
            presenter.Start();
        }
    }
}