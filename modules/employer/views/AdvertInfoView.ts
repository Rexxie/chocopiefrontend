///<reference path="../../../lib/core.d.ts"/>
///<reference path="../models/Advert.ts"/>

module Employer {
    export class AdvertInfoView {
        private layout;
        private panel;
        constructor(layout){
            this.layout = layout;
        }
        public DrawAdvertInfo(advert: Advert): void{
            this.panel = new Component('div');
            this.panel.SetAttributes('class', 'panel panel-primary');


            var panel_head = new Component('div');
            panel_head.SetAttributes('class', 'panel-heading');
            panel_head.SetContent('#' + advert.id + " Job: " + advert.jobTitle + " Salary: " +  advert.salary);

            this.panel.Append(panel_head);
            this.layout.Append(this.panel);
        }
        public DrawAdvertRecommendation(recommedndations, applyHandler:(workerid, advertId) => void): void{
            var panel_body = new Component('div');
            panel_body.SetAttributes('class', 'panel-body');

            var panel_group = new Component('div');
            panel_group.SetAttributes('class', 'panel-group');

            recommedndations.ForEach((item) => {
                var panel = new Component('div');
                panel.SetAttributes('class', 'panel panel-success');

                var panel_header = new Component('div');
                panel_header.SetAttributes('class', 'panel-heading');
                panel_header.SetContent("#" + item.workerId);

                var panel_block = new Component('div');
                panel_block.SetAttributes('class', 'panel-body');

                var name_block = new Component('div');
                name_block.SetContent("Name: " + item.name);

                var surname_block = new Component('div');
                surname_block.SetContent('Surname: ' + item.surname);

                var apply_button = new Component('button');
                apply_button.SetAttributes('class', 'btn btn-success');
                apply_button.SetContent('Apply');

                ((workerId, advertId) => {
                    apply_button.On('click', () => {
                        applyHandler(workerId, advertId);
                    });
                })(item.workerId, item.advertId);

                panel_block.Append(name_block);
                panel_block.Append(surname_block);
                panel_block.Append(apply_button);

                panel.Append(panel_header);
                panel.Append(panel_block);
                panel_group.Append(panel);
            });

            panel_body.Append(panel_group);

            this.panel.Append(panel_body);
        }
    }
}