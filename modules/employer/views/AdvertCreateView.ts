///<reference path="../../../lib/core.d.ts"/>
///<reference path="../models/Profession.ts"/>
///<reference path="../models/Skill.ts"/>

module Employer{
    export class AdvertCreateView{
        private layout:Component;
        constructor(layout){
            this.layout = layout;
        }

        public Draw(professions, skills, createHandler:(data) => void): void{
            var panel = new Component('div');
            panel.SetAttributes('class', 'panel panel-primary');


            var panel_head = new Component('div');
            panel_head.SetAttributes('class', 'panel-heading');
            panel_head.SetContent('Create Advert');


            var form = new Component('form');
            form.SetAttributes('class', 'form-horizontal');

            //var form_group = new Component('div');
            //form_group.SetAttributes('class', 'form-group');

            var form_group_title = new Component('div');
            form_group_title.SetAttributes('class', 'form-group');

            var form_group_price = new Component('div');
            form_group_price.SetAttributes('class', 'form-group');

            var form_group_professions = new Component('div');
            form_group_professions.SetAttributes('class', 'form-group');

            var form_group_skills = new Component('div');
            form_group_skills.SetAttributes('class', 'form-group');


            var title_block = new Component('div');
            title_block.SetAttributes('class', 'col-xs-12');

            var title = new Component('input');
            title.SetAttributes('class', 'form-control');
            title.SetAttributes('Placeholder', 'Title');

            title_block.Append(title);

            var price_block = new Component('div');
            price_block.SetAttributes('class', 'col-xs-12');

            var price = new Component('input');
            price.SetAttributes('class', 'form-control');
            price.SetAttributes('Placeholder', 'Salary');

            price_block.Append(price);

            /** Professions **/
            var profession_select = new Component('select');
            profession_select.SetAttributes('class', 'form-control');

            var default_profession_option = new Component('option');
            (<HTMLOptionElement>default_profession_option.element).disabled = true;
            (<HTMLOptionElement>default_profession_option.element).selected = true;

            default_profession_option.SetContent('Select profession');

            profession_select.Append(default_profession_option);

            professions.ForEach((profession: Profession) => {
                var option = new Component('option');
                option.SetContent(profession.value);
                option.SetAttributes('value', profession.id.toString());
                profession_select.Append(option);
            });

            var selected_professions_collection = new Collection<number>();
            var selected_professions = new Component('div');
            selected_professions.SetAttributes('class', 'form-control');
            selected_professions.SetContent('Professions: ');

            profession_select.On('change', (self) => {
                var select = self.element;
                if(!selected_professions_collection.Has(parseInt(select.options[select.selectedIndex].value))){
                    var text = new Component('span');
                    text.SetContent(select.options[select.selectedIndex].innerHTML);
                    var icon = new Component('span');

                    icon.SetAttributes('class', 'glyphicon glyphicon-remove');
                    ((text, icon, id) => {
                        icon.On('click', () => {
                            selected_professions_collection.Remove(id);
                            selected_professions.element.removeChild(text.element);
                            selected_professions.element.removeChild(icon.element);
                            console.log('delete', selected_professions_collection.ToArray());
                        });
                    })(text, icon, parseInt(select.options[select.selectedIndex].value));

                    selected_professions.Append(text);
                    selected_professions.Append(icon);

                    selected_professions_collection.Add(parseInt(select.options[select.selectedIndex].value));
                }
            });

            /* Skills */

            var skill_select = new Component('select');
            skill_select.SetAttributes('class', 'form-control');

            var default_skill_option = new Component('option');
            (<HTMLOptionElement>default_skill_option.element).disabled = true;
            (<HTMLOptionElement>default_skill_option.element).selected = true;

            default_skill_option.SetContent('Select skills');

            skill_select.Append(default_skill_option);


            skills.ForEach((skill: Skill) => {
                var option = new Component('option');
                option.SetContent(skill.value);
                option.SetAttributes('value', skill.id.toString());
                skill_select.Append(option);
            });

            var selected_skills_collection = new Collection<number>();
            var selected_skills = new Component('div');
            selected_skills.SetContent('Skills: ');

            skill_select.On('change', (self) => {
                var select = self.element;
                if(!selected_skills_collection.Has(parseInt(select.options[select.selectedIndex].value))){
                    var text = new Component('span');
                    text.SetContent(select.options[select.selectedIndex].innerHTML);
                    var icon = new Component('span');

                    icon.SetAttributes('class', 'glyphicon glyphicon-remove');
                    ((text, icon, id) => {
                        icon.On('click', () => {
                            selected_skills_collection.Remove(id);
                            selected_skills.element.removeChild(text.element);
                            selected_skills.element.removeChild(icon.element);
                            console.log('delete', selected_skills_collection.ToArray());
                        });
                    })(text, icon, parseInt(select.options[select.selectedIndex].value));

                    selected_skills.Append(text);
                    selected_skills.Append(icon);

                    selected_skills_collection.Add(parseInt(select.options[select.selectedIndex].value));
                }
            });

            form_group_title.Append(title_block);
            form_group_price.Append(price_block);

            form_group_professions.Append(selected_professions);
            form_group_professions.Append(profession_select);
            form_group_skills.Append(selected_skills);
            form_group_skills.Append(skill_select);

            var create_button = new Component('button');
            create_button.SetContent('Create advert');
            create_button.SetAttributes('class', 'btn btn-success');
            create_button.On('click', () => {
                createHandler({
                    title: (<HTMLInputElement>title.element).value,
                    price: parseInt((<HTMLInputElement>price.element).value),
                    professions: selected_professions_collection.ToArray(),
                    skills: selected_skills_collection.ToArray()
                });
            });

            form.Append(form_group_title);
            form.Append(form_group_price);
            form.Append(form_group_professions);
            form.Append(form_group_skills);

            var panel_body = new Component('div');
            panel_body.SetAttributes('class', 'panel-body');
            panel_body.Append(form);

            panel.Append(panel_head);
            panel.Append(panel_body);
            this.layout.Append(panel);
            //this.layout.Append(form);
            this.layout.Append(create_button);
        }
        public CreatedNotification(): void{
            Notification.GetInstance().Notify('success', 'Created', 1000);
        }
    }
}