///<reference path="../../../lib/core.d.ts"/>
///<reference path="../models/Advert.ts"/>

module Employer {
    export class AdvertsView {
        private layout: Component;
        constructor(layout: Component){
            this.layout = layout;
        }
        public Load(): void{
        }
        DrawContent(adverts: Collection<Advert>, removeHandler:(advertId: number) => void): void{
            var panels = new Component('div');
            panels.SetAttributes('class', 'panel-group');

            adverts.ForEach((advert) => {
                var panel = new Component('div');
                panel.SetAttributes('class', 'panel panel-info');

                var panel_head = new Component('div');
                panel_head.SetAttributes('class', 'panel-heading');
                panel_head.SetContent('#' + advert.id);

                var panel_body = new Component('div');
                panel_body.SetAttributes('class', 'panel-body');

                panel_body.SetContent("Job: " + advert.jobTitle + " Salary: " +  advert.salary + " Date: " + advert.date);

                var panel_footer = new Component('div');
                panel_footer.SetAttributes('class', 'panel-footer');

                var details_button = new Component('button');
                details_button.SetAttributes('class', 'btn btn-info');
                details_button.SetContent('Details');

                var button_group = new Component('div');
                button_group.SetAttributes('class', 'btn-group');

                var delete_button = new Component('button');
                delete_button.SetAttributes('class', 'btn btn-danger');
                delete_button.SetContent('Delete');


                button_group.Append(details_button);
                button_group.Append(delete_button);

                panel_footer.Append(button_group);

                panel.Append(panel_head);
                panel.Append(panel_body);
                panel.Append(panel_footer);
                panels.Append(panel);

                ((id) => {
                    details_button.On('click', () => {
                        Router.GetInstance().Go('employer', 'advertInfo', {id: id});
                    });
                    delete_button.On('click', () => {
                       removeHandler(id);
                    });
                })(advert.id);
            });
            this.layout.Append(panels);
        }
        public DeletedNotification(): void{
            Notification.GetInstance().Notify('success', "Deleted", 1000);
        }
    }
}