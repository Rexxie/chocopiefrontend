///<reference path="../../../lib/core.d.ts"/>
///<reference path="../models/Profession.ts"/>
///<reference path="../models/Skill.ts"/>

module Employer{
    export class SkillMapper{
        public Load(handler:(professions) => void): void{
            new HttpClient().Execute({
                url: 'data/skills.json',
                method: 'GET',
                callback: (data) => {
                    handler(this.BuildCollection(data));
                }
            });
        }
        private BuildObject(jsonData): Skill{
            if(jsonData){
                var data = jsonData.data;
                return new Skill({
                    id: data.id,
                    value: data.value
                });
            }
            return null
        }
        private BuildCollection(jsonData): Collection<Skill>{
            var collection;
            if(jsonData){
                collection = new Collection<Skill>();
                var array = jsonData.collection;
                for(var key in jsonData.collection){
                    collection.Add(this.BuildObject(array[key]));
                }
            }
            return collection;
        }
    }
}
