///<reference path="../../../lib/core.d.ts"/>
///<reference path="../models/Profession.ts"/>

module Employer{
    export class ProfessionMapper{
        public Load(handler:(professions) => void): void{
            new HttpClient().Execute({
                url: 'data/professions.json',
                method: 'GET',
                callback: (data) => {
                    handler(this.BuildCollection(data));
                }
            });
        }
        public BuildObject(jsonData): Profession{
            if(jsonData){
                var data = jsonData.data;
                return new Profession({
                    id: data.id,
                    value: data.value
                });
            }
            return null
        }
        public BuildCollection(jsonData): Collection<Profession>{
            var collection;
            if(jsonData){
                collection = new Collection<Profession>();
                var array = jsonData.collection;
                for(var key in jsonData.collection){
                    collection.Add(this.BuildObject(array[key]));
                }
            }
            return collection;
        }
    }
}
