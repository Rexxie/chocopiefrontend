///<reference path="../../../lib/core.d.ts"/>
///<reference path="../models/Recommendation.ts"/>

module Employer {
    export class RecommendationMapper {
        public Load(id: number, handler:(recommendation) => void): void{
            new HttpClient().Execute({
                url: 'data/advert_recommendation.json',
                method: 'GET',
                callback: (data) => {
                    handler(this.BuildCollection(data));
                }
            });
        }
        public BuildObject(jsonData):Recommendation {
            if(jsonData){
                var data = jsonData.data;
                return new Recommendation({
                    workerId: data.workerId,
                    advertId: data.advertId,
                    name: data.name,
                    surname: data.surname
                });
            }
            return null;
        }

        public BuildCollection(jsonData):Collection<Recommendation> {
            var collection;
            if (jsonData) {
                collection = new Collection<Recommendation>();
                var array = jsonData.collection;
                for (var key in array) {
                    collection.Add(this.BuildObject(array[key]))
                }
            }

            return collection;
        }
    }
}