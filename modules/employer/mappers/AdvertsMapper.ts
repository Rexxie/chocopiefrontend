///<reference path="../../../lib/core.d.ts"/>
///<reference path="../models/Advert.ts"/>

module Employer{
    export class AdvertsMapper {
        public Load(handler:(adverts: Collection<Advert>) => void):void{
            new HttpClient().Execute({
                url: 'data/adverts.json',
                method: 'GET',
                callback: (data) => {
                    handler(this.BuildCollection(data));
                }
            });
        }
        public Get(id: number, handler: (advert) => void): void{
            new HttpClient().Execute({
                url: 'data/advert_' + id +'.json',
                method: 'GET',
                callback: (data) => {
                    handler(this.BuildModel(data));
                }
            });
        }
        public Delete(id: number, handler:() => void): void{
            new HttpClient().Execute({
                url: 'delete',
                method: 'DELETE',
                callback: () => {
                    handler();
                }
            });
        }
        public BuildModel(jsonData): Advert{
            if(jsonData) {
                var data = jsonData.data;
                return new Advert({
                    id: data.id,
                    date: data.date,
                    salary: data.salary,
                    jobTitle: data.jobTitle
                })
            }
            return null;
        }
        public BuildCollection(jsonData): Collection<Advert>{
            var collection;
            if(jsonData){
                collection = new Collection<Advert>();
                var array = jsonData.collection;
                for(var key in array) {
                    collection.Add(this.BuildModel(array[key]));
                }
            }

            return collection;
        }
    }
}
