///<reference path="../models/Advert.ts"/>
///<reference path="../services/AdvertInfoService.ts"/>
///<reference path="../views/AdvertInfoView.ts"/>

module Employer{
    export class AdvertInfoPresenter{
        private view;
        private service;
        constructor(view: AdvertInfoView, service: AdvertInfoService){
            this.view = view;
            this.service = service;
        }

        public LoadAction(): void{
            this.AdvertInfoAction();

        }
        public AdvertInfoAction(): void{
            this.service.GetAdvert((advert) => {
                this.AdvertRecommendationAction(() => {
                    this.view.DrawAdvertInfo(advert);
                });
            })
        }
        public AdvertRecommendationAction(callback:() => void): void{
            this.service.GetAdvertRecommendation((recommendations) => {
                callback();
                this.view.DrawAdvertRecommendation(recommendations, this.service.ApplyWorker);
            });
        }
    }
}
