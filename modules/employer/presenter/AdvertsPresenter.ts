///<reference path="../views/AdvertsView.ts"/>
///<reference path="../services/AdvertsService.ts"/>
module Employer {
    export class AdvertsPresenter{
        private view;
        private service;
        constructor(view: AdvertsView, service: AdvertsService){
            this.view = view;
            this.service = service;
        }

        public ShowAdvertsAction(): void{
            this.service.LoadAdverts((adverts) => {
                this.view.DrawContent(adverts, (advertId) => {
                    this.service.RemoveAdvert(advertId, () => {
                        this.view.DeletedNotification();
                    });
                });
            });
        }
    }
}