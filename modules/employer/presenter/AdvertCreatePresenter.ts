///<reference path="../views/AdvertCreateView.ts"/>
///<reference path="../services/AdvertCreateService.ts"/>
module Employer{
    export class AdvertCreatePresenter{
        private view: AdvertCreateView;
        private service: AdvertCreateService;
        constructor(view: AdvertCreateView, service: AdvertCreateService){
            this.view = view;
            this.service = service;
        }
        public Start(): void{
            this.service.LoadProfessions((professions) => {
                this.service.LoadSkills((skills) => {
                    this.view.Draw(professions, skills, (data) => {
                        this.CreateAdvert(data);
                    })
                });
            });
        }
        public CreateAdvert(advertData: { title: string; price: number; professions: Array<number>; skills: Array<number> }): void{
            this.service.CreateAdvert(advertData, () => {
                this.view.CreatedNotification();
            })
        }
    }
}
