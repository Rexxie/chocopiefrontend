module Employer{
    export class Profession{
        public id: number;
        public value: string;
        constructor(args){
            this.id = args.id;
            this.value = args.value;
        }
    }
}
