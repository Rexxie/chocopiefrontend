module Employer{
    export class Skill{
        public id: number;
        public value: string;
        constructor(args){
            this.id = args.id;
            this.value = args.value;
        }
    }
}
