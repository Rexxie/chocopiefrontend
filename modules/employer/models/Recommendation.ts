module Employer{
    export class Recommendation{
        public workerId: number;
        public advertId: number;
        public name: string;
        public surname: string;

        constructor(args){
            this.workerId = args.workerId;
            this.advertId = args.advertId;
            this.name = args.name;
            this.surname = args.surname;
        }
    }
}
