///<reference path="views/AdvertsView.ts"/>
///<reference path="services/AdvertsService.ts"/>
///<reference path="presenters/AdvertsPresenter.ts"/>
module WorkerModule{
    export class WorkerFacade{
        public ShowAdverts(layout): void{
            var view = new AdvertsView(layout);
            var service = new AdvertsService();
            var presenter = new AdvertsPresenter(view, service);
            presenter.DrawAdverts();
        }
        public ShowEdit(layout): void{

        }
    }
}