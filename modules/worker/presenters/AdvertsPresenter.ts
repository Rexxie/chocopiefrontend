///<reference path="../views/AdvertsView.ts"/>
///<reference path="../services/AdvertsService.ts"/>
module WorkerModule {
    export class AdvertsPresenter {
        private view: AdvertsView;
        private service: AdvertsService;

        constructor(view, service){
            this.view = view;
            this.service = service;
        }
        public DrawAdverts(): void {
            this.service.GetWorkerAdverts((adverts) => {
                this.view.Draw(adverts,
                    () => {
                        this.service.ApplyContract(() => {
                            this.view.ApplyNotification();
                        });
                    },
                    () => {
                        this.service.RejectContract(() => {
                            this.view.DeletedNotification();
                        });
                    });
            });
        }
    }
}