///<reference path="../../../lib/core.d.ts"/>
///<reference path="../models/WorkerAdvert.ts"/>

module WorkerModule{
    export class WorkerAdvertsMapper{
        public Load(handler:(adverts) => void): void{
            new HttpClient().Execute({
                url: 'data/worker_adverts.json',
                method: 'GET',
                callback: (data) => {
                    handler(this.BuildCollection(data));
                }
            });
        }
        public BuildModel(jsonData): WorkerAdvert{
            if(jsonData) {
                var data = jsonData.data;
                return new WorkerAdvert({
                    id: data.id,
                    date: data.date,
                    salary: data.salary,
                    jobTitle: data.jobTitle
                })
            }
            return null;
        }
        public BuildCollection(jsonData): Collection<WorkerAdvert>{
            var collection;
            if(jsonData){
                collection = new Collection<WorkerAdvert>();
                var array = jsonData.collection;
                for(var key in array) {
                    collection.Add(this.BuildModel(array[key]));
                }
            }

            return collection;
        }
    }
}