module WorkerModule{
    export class WorkerAdvert{
        public id: number;
        public date: string;
        public salary: number;
        public jobTitle: string;

        constructor(args: {id: number; date: string; salary: number; jobTitle: string}) {
            this.id = args.id;
            this.date = args.date;
            this.salary = args.salary;
            this.jobTitle = args.jobTitle;
        }
    }
}
