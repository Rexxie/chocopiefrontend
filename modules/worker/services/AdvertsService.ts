///<reference path="../mappers/WorkerAdvertsMapper.ts"/>
module WorkerModule{
    export class AdvertsService{
        //private worker_id: number;
        //constructor(worker_id){
        //    this.worker_id = worker_id;
        //}
        public GetWorkerAdverts(handler:(adverts) => void): void{
            new WorkerAdvertsMapper().Load((adverts) => {
                handler(adverts);
            });
        }
        public ApplyContract(handler:() => void): void{
            handler();
        }
        public RejectContract(handler:() => void): void{
            handler();
        }
    }
}