///<reference path="../../../lib/core.d.ts"/>

module WorkerModule{
    export class AdvertsView{
        private layout;
        constructor(layout){
            this.layout = layout;
        }
        public Draw(adverts, applyHandler: () => void, rejectHandler:() => void): void{
            var panels = new Component('div');
            panels.SetAttributes('class', 'panel-group');

            adverts.ForEach((advert) => {
                var panel = new Component('div');
                panel.SetAttributes('class', 'panel panel-info');

                var panel_head = new Component('div');
                panel_head.SetAttributes('class', 'panel-heading');
                panel_head.SetContent('#' + advert.id);

                var panel_body = new Component('div');
                panel_body.SetAttributes('class', 'panel-body');

                panel_body.SetContent("Job: " + advert.jobTitle + " Salary: " +  advert.salary + " Date: " + advert.date);

                var panel_footer = new Component('div');
                panel_footer.SetAttributes('class', 'panel-footer');

                var details_button = new Component('button');
                details_button.SetAttributes('class', 'btn btn-info');
                details_button.SetContent('Apply');

                var button_group = new Component('div');
                button_group.SetAttributes('class', 'btn-group');

                var reject_button = new Component('button');
                reject_button.SetAttributes('class', 'btn btn-danger');
                reject_button.SetContent('Reject');


                button_group.Append(details_button);
                button_group.Append(reject_button);

                panel_footer.Append(button_group);

                panel.Append(panel_head);
                panel.Append(panel_body);
                panel.Append(panel_footer);
                panels.Append(panel);

                ((id) => {
                    details_button.On('click', () => {
                        applyHandler();
                    });
                    reject_button.On('click', () => {
                        rejectHandler();
                    });
                })(advert.id);
            });
            this.layout.Append(panels);

        }
        public DeletedNotification(): void{
            Notification.GetInstance().Notify('success', "Rejected", 1000);
        }
        public ApplyNotification(): void{
            Notification.GetInstance().Notify('success', "Confirmed", 1000);
        }
    }
}
