/// <reference path="../../lib/core.d.ts" />
declare module Employer {
    class Advert {
        id: number;
        date: string;
        salary: number;
        jobTitle: string;
        constructor(args: {
            id: number;
            date: string;
            salary: number;
            jobTitle: string;
        });
    }
}
declare module Employer {
    class AdvertsView {
        private layout;
        constructor(layout: Component);
        Load(): void;
        DrawContent(adverts: Collection<Advert>, removeHandler: (advertId: number) => void): void;
        DeletedNotification(): void;
    }
}
declare module Employer {
    class AdvertsMapper {
        Load(handler: (adverts: Collection<Advert>) => void): void;
        Get(id: number, handler: (advert) => void): void;
        Delete(id: number, handler: () => void): void;
        BuildModel(jsonData: any): Advert;
        BuildCollection(jsonData: any): Collection<Advert>;
    }
}
declare module Employer {
    class AdvertsService {
        constructor();
        LoadAdverts(handler: (adverts) => void): void;
        RemoveAdvert(advertId: number, deletedHandler: () => void): void;
    }
}
declare module Employer {
    class AdvertsPresenter {
        private view;
        private service;
        constructor(view: AdvertsView, service: AdvertsService);
        ShowAdvertsAction(): void;
    }
}
declare module Employer {
    class AdvertInfoView {
        private layout;
        private panel;
        constructor(layout: any);
        DrawAdvertInfo(advert: Advert): void;
        DrawAdvertRecommendation(recommedndations: any, applyHandler: (workerid, advertId) => void): void;
    }
}
declare module Employer {
    class Recommendation {
        workerId: number;
        advertId: number;
        name: string;
        surname: string;
        constructor(args: any);
    }
}
declare module Employer {
    class RecommendationMapper {
        Load(id: number, handler: (recommendation) => void): void;
        BuildObject(jsonData: any): Recommendation;
        BuildCollection(jsonData: any): Collection<Recommendation>;
    }
}
declare module Employer {
    class AdvertInfoService {
        private advertId;
        constructor(advertId: number);
        GetAdvert(handler: (advert: Advert) => void): void;
        GetAdvertRecommendation(handler: (recommendations) => void): void;
        ApplyWorker(workerId: number, advertId: number): void;
    }
}
declare module Employer {
    class AdvertInfoPresenter {
        private view;
        private service;
        constructor(view: AdvertInfoView, service: AdvertInfoService);
        LoadAction(): void;
        AdvertInfoAction(): void;
        AdvertRecommendationAction(callback: () => void): void;
    }
}
declare module Employer {
    class Profession {
        id: number;
        value: string;
        constructor(args: any);
    }
}
declare module Employer {
    class Skill {
        id: number;
        value: string;
        constructor(args: any);
    }
}
declare module Employer {
    class AdvertCreateView {
        private layout;
        constructor(layout: any);
        Draw(professions: any, skills: any, createHandler: (data) => void): void;
        CreatedNotification(): void;
    }
}
declare module Employer {
    class ProfessionMapper {
        Load(handler: (professions) => void): void;
        BuildObject(jsonData: any): Profession;
        BuildCollection(jsonData: any): Collection<Profession>;
    }
}
declare module Employer {
    class SkillMapper {
        Load(handler: (professions) => void): void;
        private BuildObject(jsonData);
        private BuildCollection(jsonData);
    }
}
declare module Employer {
    class AdvertCreateService {
        CreateAdvert(data: any, createdHandler: () => void): void;
        LoadProfessions(handler: (professions) => void): void;
        LoadSkills(handler: (skills) => void): void;
    }
}
declare module Employer {
    class AdvertCreatePresenter {
        private view;
        private service;
        constructor(view: AdvertCreateView, service: AdvertCreateService);
        Start(): void;
        CreateAdvert(advertData: {
            title: string;
            price: number;
            professions: Array<number>;
            skills: Array<number>;
        }): void;
    }
}
declare module Employer {
    class EmployerFacade {
        constructor();
        ShowAdverts(layout: Component): void;
        ShowAdvertInfo(layout: Component, id: number): void;
        CreateAdvert(layout: Component): void;
    }
}
