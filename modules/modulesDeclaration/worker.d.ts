/// <reference path="../../lib/core.d.ts" />
declare module WorkerModule {
    class AdvertsView {
        private layout;
        constructor(layout: any);
        Draw(adverts: any, applyHandler: () => void, rejectHandler: () => void): void;
        DeletedNotification(): void;
        ApplyNotification(): void;
    }
}
declare module WorkerModule {
    class WorkerAdvert {
        id: number;
        date: string;
        salary: number;
        jobTitle: string;
        constructor(args: {
            id: number;
            date: string;
            salary: number;
            jobTitle: string;
        });
    }
}
declare module WorkerModule {
    class WorkerAdvertsMapper {
        Load(handler: (adverts) => void): void;
        BuildModel(jsonData: any): WorkerAdvert;
        BuildCollection(jsonData: any): Collection<WorkerAdvert>;
    }
}
declare module WorkerModule {
    class AdvertsService {
        GetWorkerAdverts(handler: (adverts) => void): void;
        ApplyContract(handler: () => void): void;
        RejectContract(handler: () => void): void;
    }
}
declare module WorkerModule {
    class AdvertsPresenter {
        private view;
        private service;
        constructor(view: any, service: any);
        DrawAdverts(): void;
    }
}
declare module WorkerModule {
    class WorkerFacade {
        ShowAdverts(layout: any): void;
        ShowEdit(layout: any): void;
    }
}
