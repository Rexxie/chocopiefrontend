var Collection = (function () {
    function Collection() {
        this.array = [];
    }
    Collection.prototype.Add = function (item) {
        this.array.push(item);
    };
    Collection.prototype.Remove = function (item) {
        for (var key in this.array) {
            if (this.array[key] == item) {
                this.array.splice(key, 1);
                return;
            }
        }
    };
    Collection.prototype.Has = function (value) {
        for (var _i = 0, _a = this.array; _i < _a.length; _i++) {
            var item = _a[_i];
            if (item == value) {
                return true;
            }
        }
        return false;
    };
    Collection.prototype.ForEach = function (handler) {
        for (var _i = 0, _a = this.array; _i < _a.length; _i++) {
            var item = _a[_i];
            handler(item);
        }
    };
    Collection.prototype.ToArray = function () {
        return this.array;
    };
    return Collection;
})();
var Router = (function () {
    function Router() {
        this.modules = new Collection();
        if (Router.instance) {
            throw new Error('Router is singleton');
        }
    }
    Router.GetInstance = function () {
        return Router.instance;
    };
    Router.prototype.Go = function (module, method, args) {
        this.modules.ForEach(function (item) {
            if (item.module == module) {
                if (item.method == method) {
                    if (args) {
                        return item.callback(args);
                    }
                    if (!args) {
                        return item.callback();
                    }
                }
            }
        });
    };
    Router.prototype.SetModule = function (module, method, callback) {
        if (module != null && method != null && callback != null) {
            this.modules.Add({
                module: module,
                method: method,
                callback: callback
            });
        }
    };
    Router.instance = new Router();
    return Router;
})();
var Component = (function () {
    function Component(tag) {
        this.element = document.createElement(tag);
        this._events = Array();
    }
    Component.prototype.Append = function (component) {
        this.element.appendChild(component.element);
    };
    Component.prototype.SetAttributes = function (name, value) {
        this.element.setAttribute(name, value);
    };
    Component.prototype.SetContent = function (content) {
        this.element.innerHTML = content;
    };
    Component.prototype.Clear = function () {
        this.element.innerHTML = '';
    };
    Component.prototype.On = function (eventName, callback) {
        var _this = this;
        for (var key in this._events) {
            if (this._events[key].name == eventName) {
                this.element.addEventListener(eventName, function (e) {
                    callback(_this);
                }, false);
                return;
            }
        }
        var event = document.createEvent('Event');
        event.initEvent(eventName, true, true);
        this.element.addEventListener(eventName, function (e) {
            e.stopPropagation();
            callback(_this);
        }, false);
        this._events.push({ name: eventName, event: event });
    };
    Component.prototype.Trigger = function (eventName) {
        for (var key in this._events) {
            if (eventName == this._events[key].name) {
                this.element.dispatchEvent(this._events[key].event);
                return;
            }
        }
    };
    return Component;
})();
var Body = (function () {
    function Body() {
        this.body = document.body;
        if (Body.instance) {
            throw new Error('Body');
        }
    }
    Body.GetInstance = function () {
        return Body.instance;
    };
    Body.prototype.Append = function (component) {
        this.body.appendChild(component.element);
    };
    Body.prototype.GetBody = function () {
        return this.body;
    };
    Body.instance = new Body();
    return Body;
})();
var HttpRequest = (function () {
    function HttpRequest() {
        this.httpRequest = new XMLHttpRequest();
    }
    HttpRequest.prototype.Request = function (args) {
        var _this = this;
        this.httpRequest = new XMLHttpRequest();
        this.httpRequest.open(args.method, args.url, true);
        for (var key in args.headers) {
            this.httpRequest.setRequestHeader(key, args.headers[key]);
        }
        this.httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        this.httpRequest.onloadend = function () {
            if (_this.httpRequest.status == 200) {
                return args.handler(JSON.parse(_this.httpRequest.responseText));
            }
            if (_this.httpRequest.status == 204) {
                return args.handler();
            }
        };
        if (args.errorHandler) {
            this.httpRequest.onerror = function () {
                args.errorHandler(_this.httpRequest.statusText);
            };
        }
        this.httpRequest.send(args.data);
    };
    HttpRequest.prototype.OnError = function (handler) {
        var _this = this;
        this.httpRequest.addEventListener('error', function () {
            handler(_this.httpRequest.statusText);
        });
    };
    return HttpRequest;
})();
var HttpClient = (function () {
    function HttpClient() {
        this.http = new HttpRequest();
    }
    HttpClient.prototype.GetRequest = function (url) {
    };
    HttpClient.prototype.PostRequest = function (url, data) {
    };
    HttpClient.prototype.Execute = function (args) {
        this.http.Request({
            url: args.url,
            method: args.method,
            headers: args.headers,
            handler: args.callback,
            data: args.data
        });
    };
    return HttpClient;
})();
var ScriptLoader = (function () {
    function ScriptLoader(path) {
        this.script = new Component('script');
        this.script.SetAttributes('src', path);
    }
    ScriptLoader.prototype.Load = function (handler) {
        if (handler) {
            this.script.On('load', function () {
                handler();
            });
        }
        Body.GetInstance().Append(this.script);
    };
    return ScriptLoader;
})();
var Notification = (function () {
    function Notification() {
        this.notification = new Component('div');
        if (Notification.instance) {
            throw new Error('Notification');
        }
    }
    Notification.GetInstance = function () {
        return Notification.instance;
    };
    Notification.prototype.GetNotification = function () {
        return this.notification;
    };
    Notification.prototype.Notify = function (notifyType, message, time) {
        var _this = this;
        switch (notifyType) {
            case "success":
                this.notification.SetAttributes('class', 'alert alert-success');
                break;
        }
        this.notification.SetContent(message);
        console.log(time);
        if (time) {
            setTimeout(function () {
                _this.Close();
            }, time);
        }
        if (!time) {
            var close = new Component('a');
            close.SetAttributes('class', 'close');
            close.SetContent('&times;');
            close.On('click', function () {
                _this.Close();
            });
            this.notification.Append(close);
        }
    };
    Notification.prototype.Close = function () {
        this.notification.Clear();
        this.notification.SetAttributes('class', 'close');
    };
    Notification.instance = new Notification();
    return Notification;
})();
