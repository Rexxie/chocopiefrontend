var Application;
(function (Application) {
    var ContainerView = (function () {
        function ContainerView(body) {
            this.container = new Component('div');
            this.container.SetAttributes('class', 'row');
            this.notification = Notification.GetInstance().GetNotification();
            body.Append(this.notification);
            body.Append(this.container);
        }
        ContainerView.prototype.Append = function (component) {
            this.container.Append(component);
        };
        ContainerView.prototype.Clear = function () {
            this.container.Clear();
        };
        return ContainerView;
    })();
    Application.ContainerView = ContainerView;
})(Application || (Application = {}));
var Application;
(function (Application) {
    var NavigationView = (function () {
        function NavigationView(body) {
            var nav = new Component('nav');
            nav.SetAttributes('class', 'navbar navbar-inverse');
            var nav_container = new Component('div');
            nav_container.SetAttributes('class', 'container-fluid');
            this.container = new Component('ul');
            this.container.SetAttributes('class', 'nav navbar-nav');
            nav_container.Append(this.container);
            nav.Append(nav_container);
            body.Append(nav);
        }
        NavigationView.prototype.AddItem = function (title, handler) {
            var item = new Component('li');
            var a = new Component('a');
            a.SetContent(title);
            a.On('click', function () {
                handler();
            });
            item.Append(a);
            this.container.Append(item);
        };
        return NavigationView;
    })();
    Application.NavigationView = NavigationView;
})(Application || (Application = {}));
var Application;
(function (Application) {
    var AppPage = (function () {
        function AppPage() {
            this.navigation = new Application.NavigationView(Body.GetInstance());
            this.container = new Application.ContainerView(Body.GetInstance());
        }
        AppPage.prototype.Append = function (element) {
            this.container.Append(element);
        };
        AppPage.prototype.Clear = function () {
            this.container.Clear();
        };
        AppPage.prototype.GetNavigationView = function () {
            return this.navigation;
        };
        return AppPage;
    })();
    Application.AppPage = AppPage;
})(Application || (Application = {}));
var Application;
(function (Application) {
    var AppFacade = (function () {
        function AppFacade(page) {
            this.loadedModules = new Collection();
            this.page = page;
        }
        AppFacade.prototype.EmployerInit = function (handler) {
            var _this = this;
            if (!this.loadedModules.Has('employer')) {
                new ScriptLoader('build/employer.js').Load(function () {
                    _this.loadedModules.Add('employer');
                    handler();
                });
            }
        };
        AppFacade.prototype.EmployerShowAdverts = function () {
            this.page.Clear();
            var empLayout = new Component('div');
            empLayout.SetAttributes('class', 'col-sm-6 col-centered');
            this.page.Append(empLayout);
            if (this.loadedModules.Has('employer')) {
                new Employer.EmployerFacade().ShowAdverts(empLayout);
            }
        };
        AppFacade.prototype.EmployerAdvertInfo = function (id) {
            this.page.Clear();
            var empLayout = new Component('div');
            empLayout.SetAttributes('class', 'col-sm-4');
            this.page.Append(empLayout);
            if (this.loadedModules.Has('employer')) {
                new Employer.EmployerFacade().ShowAdvertInfo(empLayout, id);
            }
        };
        AppFacade.prototype.EmployerCreateAdvert = function () {
            this.page.Clear();
            var empLayout = new Component('div');
            empLayout.SetAttributes('class', 'col-sm-6 col-centered');
            this.page.Append(empLayout);
            if (this.loadedModules.Has('employer')) {
                new Employer.EmployerFacade().CreateAdvert(empLayout);
            }
        };
        AppFacade.prototype.WorkerInit = function (handler) {
            var _this = this;
            if (!this.loadedModules.Has('worker')) {
                new ScriptLoader('build/worker.js').Load(function () {
                    _this.loadedModules.Add('worker');
                    handler();
                });
            }
        };
        AppFacade.prototype.WorkerAdverts = function () {
            this.page.Clear();
            var workerLayout = new Component('div');
            workerLayout.SetAttributes('class', 'col-sm-6 col-centered');
            this.page.Append(workerLayout);
            if (this.loadedModules.Has('worker')) {
                new WorkerModule.WorkerFacade().ShowAdverts(workerLayout);
            }
        };
        AppFacade.prototype.WorkerEdit = function () {
            this.page.Clear();
            var workerLayout = new Component('div');
            workerLayout.SetAttributes('class', 'col-sm-6 col-centered');
            this.page.Append(workerLayout);
            if (this.loadedModules.Has('worker')) {
                new WorkerModule.WorkerFacade().ShowEdit(workerLayout);
            }
        };
        return AppFacade;
    })();
    Application.AppFacade = AppFacade;
})(Application || (Application = {}));
var Application;
(function (Application) {
    var User = (function () {
        function User(args) {
            this.login = args.login;
            this.userType = args.userType;
        }
        return User;
    })();
    Application.User = User;
})(Application || (Application = {}));
var Application;
(function (Application) {
    var UserMapper = (function () {
        function UserMapper() {
        }
        UserMapper.prototype.GetCurrentUser = function (handler) {
            var _this = this;
            new HttpClient().Execute({
                url: 'data/user.json',
                method: 'GET',
                callback: function (data) {
                    handler(_this.BuildModel(data));
                }
            });
        };
        UserMapper.prototype.BuildModel = function (jsonData) {
            if (jsonData) {
                var data = jsonData.data;
                return new Application.User({
                    login: data.login,
                    userType: data.userType
                });
            }
            return null;
        };
        return UserMapper;
    })();
    Application.UserMapper = UserMapper;
})(Application || (Application = {}));
var Application;
(function (Application_1) {
    var Application = (function () {
        function Application() {
        }
        Application.Start = function () {
            var page = new Application_1.AppPage();
            var navigation = page.GetNavigationView();
            var facade = new Application_1.AppFacade(page);
            var router = Router.GetInstance();
            new Application_1.UserMapper().GetCurrentUser(function (user) {
                if (user.userType == 'Employer') {
                    router.SetModule('employer', 'adverts', function () {
                        facade.EmployerShowAdverts();
                    });
                    router.SetModule('employer', 'advertInfo', function (args) {
                        facade.EmployerAdvertInfo(args.id);
                    });
                    router.SetModule('employer', 'createAdvert', function () {
                        facade.EmployerCreateAdvert();
                    });
                    router.SetModule('employer', 'init', function () {
                        facade.EmployerInit(function () {
                            router.Go('employer', 'adverts');
                        });
                    });
                    navigation.AddItem('Adverts', function () {
                        router.Go('employer', 'adverts');
                    });
                    navigation.AddItem('Create Advert', function () {
                        router.Go('employer', 'createAdvert');
                    });
                    return router.Go('employer', 'init');
                }
                if (user.userType == 'Worker') {
                    router.SetModule('worker', 'adverts', function () {
                        facade.WorkerAdverts();
                    });
                    router.SetModule('worker', 'edit', function () {
                        facade.WorkerEdit();
                    });
                    router.SetModule('worker', 'init', function () {
                        facade.WorkerInit(function () {
                            router.Go('worker', 'adverts');
                        });
                    });
                    navigation.AddItem('Adverts', function () {
                        router.Go('worker', 'adverts');
                    });
                    navigation.AddItem('Edit', function () {
                        router.Go('worker', 'edit');
                    });
                    return router.Go('worker', 'init');
                }
                if (user.userType == 'admin') {
                    return router.Go('admin', 'init');
                }
            });
        };
        return Application;
    })();
    Application_1.Application = Application;
})(Application || (Application = {}));
