var WorkerModule;
(function (WorkerModule) {
    var AdvertsView = (function () {
        function AdvertsView(layout) {
            this.layout = layout;
        }
        AdvertsView.prototype.Draw = function (adverts, applyHandler, rejectHandler) {
            var panels = new Component('div');
            panels.SetAttributes('class', 'panel-group');
            adverts.ForEach(function (advert) {
                var panel = new Component('div');
                panel.SetAttributes('class', 'panel panel-info');
                var panel_head = new Component('div');
                panel_head.SetAttributes('class', 'panel-heading');
                panel_head.SetContent('#' + advert.id);
                var panel_body = new Component('div');
                panel_body.SetAttributes('class', 'panel-body');
                panel_body.SetContent("Job: " + advert.jobTitle + " Salary: " + advert.salary + " Date: " + advert.date);
                var panel_footer = new Component('div');
                panel_footer.SetAttributes('class', 'panel-footer');
                var details_button = new Component('button');
                details_button.SetAttributes('class', 'btn btn-info');
                details_button.SetContent('Apply');
                var button_group = new Component('div');
                button_group.SetAttributes('class', 'btn-group');
                var reject_button = new Component('button');
                reject_button.SetAttributes('class', 'btn btn-danger');
                reject_button.SetContent('Reject');
                button_group.Append(details_button);
                button_group.Append(reject_button);
                panel_footer.Append(button_group);
                panel.Append(panel_head);
                panel.Append(panel_body);
                panel.Append(panel_footer);
                panels.Append(panel);
                (function (id) {
                    details_button.On('click', function () {
                        applyHandler();
                    });
                    reject_button.On('click', function () {
                        rejectHandler();
                    });
                })(advert.id);
            });
            this.layout.Append(panels);
        };
        AdvertsView.prototype.DeletedNotification = function () {
            Notification.GetInstance().Notify('success', "Rejected", 1000);
        };
        AdvertsView.prototype.ApplyNotification = function () {
            Notification.GetInstance().Notify('success', "Confirmed", 1000);
        };
        return AdvertsView;
    })();
    WorkerModule.AdvertsView = AdvertsView;
})(WorkerModule || (WorkerModule = {}));
var WorkerModule;
(function (WorkerModule) {
    var WorkerAdvert = (function () {
        function WorkerAdvert(args) {
            this.id = args.id;
            this.date = args.date;
            this.salary = args.salary;
            this.jobTitle = args.jobTitle;
        }
        return WorkerAdvert;
    })();
    WorkerModule.WorkerAdvert = WorkerAdvert;
})(WorkerModule || (WorkerModule = {}));
var WorkerModule;
(function (WorkerModule) {
    var WorkerAdvertsMapper = (function () {
        function WorkerAdvertsMapper() {
        }
        WorkerAdvertsMapper.prototype.Load = function (handler) {
            var _this = this;
            new HttpClient().Execute({
                url: 'data/worker_adverts.json',
                method: 'GET',
                callback: function (data) {
                    handler(_this.BuildCollection(data));
                }
            });
        };
        WorkerAdvertsMapper.prototype.BuildModel = function (jsonData) {
            if (jsonData) {
                var data = jsonData.data;
                return new WorkerModule.WorkerAdvert({
                    id: data.id,
                    date: data.date,
                    salary: data.salary,
                    jobTitle: data.jobTitle
                });
            }
            return null;
        };
        WorkerAdvertsMapper.prototype.BuildCollection = function (jsonData) {
            var collection;
            if (jsonData) {
                collection = new Collection();
                var array = jsonData.collection;
                for (var key in array) {
                    collection.Add(this.BuildModel(array[key]));
                }
            }
            return collection;
        };
        return WorkerAdvertsMapper;
    })();
    WorkerModule.WorkerAdvertsMapper = WorkerAdvertsMapper;
})(WorkerModule || (WorkerModule = {}));
var WorkerModule;
(function (WorkerModule) {
    var AdvertsService = (function () {
        function AdvertsService() {
        }
        AdvertsService.prototype.GetWorkerAdverts = function (handler) {
            new WorkerModule.WorkerAdvertsMapper().Load(function (adverts) {
                handler(adverts);
            });
        };
        AdvertsService.prototype.ApplyContract = function (handler) {
            handler();
        };
        AdvertsService.prototype.RejectContract = function (handler) {
            handler();
        };
        return AdvertsService;
    })();
    WorkerModule.AdvertsService = AdvertsService;
})(WorkerModule || (WorkerModule = {}));
var WorkerModule;
(function (WorkerModule) {
    var AdvertsPresenter = (function () {
        function AdvertsPresenter(view, service) {
            this.view = view;
            this.service = service;
        }
        AdvertsPresenter.prototype.DrawAdverts = function () {
            var _this = this;
            this.service.GetWorkerAdverts(function (adverts) {
                _this.view.Draw(adverts, function () {
                    _this.service.ApplyContract(function () {
                        _this.view.ApplyNotification();
                    });
                }, function () {
                    _this.service.RejectContract(function () {
                        _this.view.DeletedNotification();
                    });
                });
            });
        };
        return AdvertsPresenter;
    })();
    WorkerModule.AdvertsPresenter = AdvertsPresenter;
})(WorkerModule || (WorkerModule = {}));
var WorkerModule;
(function (WorkerModule) {
    var WorkerFacade = (function () {
        function WorkerFacade() {
        }
        WorkerFacade.prototype.ShowAdverts = function (layout) {
            var view = new WorkerModule.AdvertsView(layout);
            var service = new WorkerModule.AdvertsService();
            var presenter = new WorkerModule.AdvertsPresenter(view, service);
            presenter.DrawAdverts();
        };
        WorkerFacade.prototype.ShowEdit = function (layout) {
        };
        return WorkerFacade;
    })();
    WorkerModule.WorkerFacade = WorkerFacade;
})(WorkerModule || (WorkerModule = {}));
