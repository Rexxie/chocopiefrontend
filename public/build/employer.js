var Employer;
(function (Employer) {
    var Advert = (function () {
        function Advert(args) {
            this.id = args.id;
            this.date = args.date;
            this.salary = args.salary;
            this.jobTitle = args.jobTitle;
        }
        return Advert;
    })();
    Employer.Advert = Advert;
})(Employer || (Employer = {}));
var Employer;
(function (Employer) {
    var AdvertsView = (function () {
        function AdvertsView(layout) {
            this.layout = layout;
        }
        AdvertsView.prototype.Load = function () {
        };
        AdvertsView.prototype.DrawContent = function (adverts, removeHandler) {
            var panels = new Component('div');
            panels.SetAttributes('class', 'panel-group');
            adverts.ForEach(function (advert) {
                var panel = new Component('div');
                panel.SetAttributes('class', 'panel panel-info');
                var panel_head = new Component('div');
                panel_head.SetAttributes('class', 'panel-heading');
                panel_head.SetContent('#' + advert.id);
                var panel_body = new Component('div');
                panel_body.SetAttributes('class', 'panel-body');
                panel_body.SetContent("Job: " + advert.jobTitle + " Salary: " + advert.salary + " Date: " + advert.date);
                var panel_footer = new Component('div');
                panel_footer.SetAttributes('class', 'panel-footer');
                var details_button = new Component('button');
                details_button.SetAttributes('class', 'btn btn-info');
                details_button.SetContent('Details');
                var button_group = new Component('div');
                button_group.SetAttributes('class', 'btn-group');
                var delete_button = new Component('button');
                delete_button.SetAttributes('class', 'btn btn-danger');
                delete_button.SetContent('Delete');
                button_group.Append(details_button);
                button_group.Append(delete_button);
                panel_footer.Append(button_group);
                panel.Append(panel_head);
                panel.Append(panel_body);
                panel.Append(panel_footer);
                panels.Append(panel);
                (function (id) {
                    details_button.On('click', function () {
                        Router.GetInstance().Go('employer', 'advertInfo', { id: id });
                    });
                    delete_button.On('click', function () {
                        removeHandler(id);
                    });
                })(advert.id);
            });
            this.layout.Append(panels);
        };
        AdvertsView.prototype.DeletedNotification = function () {
            Notification.GetInstance().Notify('success', "Deleted", 1000);
        };
        return AdvertsView;
    })();
    Employer.AdvertsView = AdvertsView;
})(Employer || (Employer = {}));
var Employer;
(function (Employer) {
    var AdvertsMapper = (function () {
        function AdvertsMapper() {
        }
        AdvertsMapper.prototype.Load = function (handler) {
            var _this = this;
            new HttpClient().Execute({
                url: 'data/adverts.json',
                method: 'GET',
                callback: function (data) {
                    handler(_this.BuildCollection(data));
                }
            });
        };
        AdvertsMapper.prototype.Get = function (id, handler) {
            var _this = this;
            new HttpClient().Execute({
                url: 'data/advert_' + id + '.json',
                method: 'GET',
                callback: function (data) {
                    handler(_this.BuildModel(data));
                }
            });
        };
        AdvertsMapper.prototype.Delete = function (id, handler) {
            new HttpClient().Execute({
                url: 'delete',
                method: 'DELETE',
                callback: function () {
                    handler();
                }
            });
        };
        AdvertsMapper.prototype.BuildModel = function (jsonData) {
            if (jsonData) {
                var data = jsonData.data;
                return new Employer.Advert({
                    id: data.id,
                    date: data.date,
                    salary: data.salary,
                    jobTitle: data.jobTitle
                });
            }
            return null;
        };
        AdvertsMapper.prototype.BuildCollection = function (jsonData) {
            var collection;
            if (jsonData) {
                collection = new Collection();
                var array = jsonData.collection;
                for (var key in array) {
                    collection.Add(this.BuildModel(array[key]));
                }
            }
            return collection;
        };
        return AdvertsMapper;
    })();
    Employer.AdvertsMapper = AdvertsMapper;
})(Employer || (Employer = {}));
var Employer;
(function (Employer) {
    var AdvertsService = (function () {
        function AdvertsService() {
        }
        AdvertsService.prototype.LoadAdverts = function (handler) {
            new Employer.AdvertsMapper().Load(function (adverts) {
                handler(adverts);
            });
        };
        AdvertsService.prototype.RemoveAdvert = function (advertId, deletedHandler) {
            deletedHandler();
        };
        return AdvertsService;
    })();
    Employer.AdvertsService = AdvertsService;
})(Employer || (Employer = {}));
var Employer;
(function (Employer) {
    var AdvertsPresenter = (function () {
        function AdvertsPresenter(view, service) {
            this.view = view;
            this.service = service;
        }
        AdvertsPresenter.prototype.ShowAdvertsAction = function () {
            var _this = this;
            this.service.LoadAdverts(function (adverts) {
                _this.view.DrawContent(adverts, function (advertId) {
                    _this.service.RemoveAdvert(advertId, function () {
                        _this.view.DeletedNotification();
                    });
                });
            });
        };
        return AdvertsPresenter;
    })();
    Employer.AdvertsPresenter = AdvertsPresenter;
})(Employer || (Employer = {}));
var Employer;
(function (Employer) {
    var AdvertInfoView = (function () {
        function AdvertInfoView(layout) {
            this.layout = layout;
        }
        AdvertInfoView.prototype.DrawAdvertInfo = function (advert) {
            this.panel = new Component('div');
            this.panel.SetAttributes('class', 'panel panel-primary');
            var panel_head = new Component('div');
            panel_head.SetAttributes('class', 'panel-heading');
            panel_head.SetContent('#' + advert.id + " Job: " + advert.jobTitle + " Salary: " + advert.salary);
            this.panel.Append(panel_head);
            this.layout.Append(this.panel);
        };
        AdvertInfoView.prototype.DrawAdvertRecommendation = function (recommedndations, applyHandler) {
            var panel_body = new Component('div');
            panel_body.SetAttributes('class', 'panel-body');
            var panel_group = new Component('div');
            panel_group.SetAttributes('class', 'panel-group');
            recommedndations.ForEach(function (item) {
                var panel = new Component('div');
                panel.SetAttributes('class', 'panel panel-success');
                var panel_header = new Component('div');
                panel_header.SetAttributes('class', 'panel-heading');
                panel_header.SetContent("#" + item.workerId);
                var panel_block = new Component('div');
                panel_block.SetAttributes('class', 'panel-body');
                var name_block = new Component('div');
                name_block.SetContent("Name: " + item.name);
                var surname_block = new Component('div');
                surname_block.SetContent('Surname: ' + item.surname);
                var apply_button = new Component('button');
                apply_button.SetAttributes('class', 'btn btn-success');
                apply_button.SetContent('Apply');
                (function (workerId, advertId) {
                    apply_button.On('click', function () {
                        applyHandler(workerId, advertId);
                    });
                })(item.workerId, item.advertId);
                panel_block.Append(name_block);
                panel_block.Append(surname_block);
                panel_block.Append(apply_button);
                panel.Append(panel_header);
                panel.Append(panel_block);
                panel_group.Append(panel);
            });
            panel_body.Append(panel_group);
            this.panel.Append(panel_body);
        };
        return AdvertInfoView;
    })();
    Employer.AdvertInfoView = AdvertInfoView;
})(Employer || (Employer = {}));
var Employer;
(function (Employer) {
    var Recommendation = (function () {
        function Recommendation(args) {
            this.workerId = args.workerId;
            this.advertId = args.advertId;
            this.name = args.name;
            this.surname = args.surname;
        }
        return Recommendation;
    })();
    Employer.Recommendation = Recommendation;
})(Employer || (Employer = {}));
var Employer;
(function (Employer) {
    var RecommendationMapper = (function () {
        function RecommendationMapper() {
        }
        RecommendationMapper.prototype.Load = function (id, handler) {
            var _this = this;
            new HttpClient().Execute({
                url: 'data/advert_recommendation.json',
                method: 'GET',
                callback: function (data) {
                    handler(_this.BuildCollection(data));
                }
            });
        };
        RecommendationMapper.prototype.BuildObject = function (jsonData) {
            if (jsonData) {
                var data = jsonData.data;
                return new Employer.Recommendation({
                    workerId: data.workerId,
                    advertId: data.advertId,
                    name: data.name,
                    surname: data.surname
                });
            }
            return null;
        };
        RecommendationMapper.prototype.BuildCollection = function (jsonData) {
            var collection;
            if (jsonData) {
                collection = new Collection();
                var array = jsonData.collection;
                for (var key in array) {
                    collection.Add(this.BuildObject(array[key]));
                }
            }
            return collection;
        };
        return RecommendationMapper;
    })();
    Employer.RecommendationMapper = RecommendationMapper;
})(Employer || (Employer = {}));
var Employer;
(function (Employer) {
    var AdvertInfoService = (function () {
        function AdvertInfoService(advertId) {
            this.advertId = advertId;
        }
        AdvertInfoService.prototype.GetAdvert = function (handler) {
            new Employer.AdvertsMapper().Get(this.advertId, function (advert) {
                handler(advert);
            });
        };
        AdvertInfoService.prototype.GetAdvertRecommendation = function (handler) {
            new Employer.RecommendationMapper().Load(this.advertId, function (recommendations) {
                handler(recommendations);
            });
        };
        AdvertInfoService.prototype.ApplyWorker = function (workerId, advertId) {
        };
        return AdvertInfoService;
    })();
    Employer.AdvertInfoService = AdvertInfoService;
})(Employer || (Employer = {}));
var Employer;
(function (Employer) {
    var AdvertInfoPresenter = (function () {
        function AdvertInfoPresenter(view, service) {
            this.view = view;
            this.service = service;
        }
        AdvertInfoPresenter.prototype.LoadAction = function () {
            this.AdvertInfoAction();
        };
        AdvertInfoPresenter.prototype.AdvertInfoAction = function () {
            var _this = this;
            this.service.GetAdvert(function (advert) {
                _this.AdvertRecommendationAction(function () {
                    _this.view.DrawAdvertInfo(advert);
                });
            });
        };
        AdvertInfoPresenter.prototype.AdvertRecommendationAction = function (callback) {
            var _this = this;
            this.service.GetAdvertRecommendation(function (recommendations) {
                callback();
                _this.view.DrawAdvertRecommendation(recommendations, _this.service.ApplyWorker);
            });
        };
        return AdvertInfoPresenter;
    })();
    Employer.AdvertInfoPresenter = AdvertInfoPresenter;
})(Employer || (Employer = {}));
var Employer;
(function (Employer) {
    var Profession = (function () {
        function Profession(args) {
            this.id = args.id;
            this.value = args.value;
        }
        return Profession;
    })();
    Employer.Profession = Profession;
})(Employer || (Employer = {}));
var Employer;
(function (Employer) {
    var Skill = (function () {
        function Skill(args) {
            this.id = args.id;
            this.value = args.value;
        }
        return Skill;
    })();
    Employer.Skill = Skill;
})(Employer || (Employer = {}));
var Employer;
(function (Employer) {
    var AdvertCreateView = (function () {
        function AdvertCreateView(layout) {
            this.layout = layout;
        }
        AdvertCreateView.prototype.Draw = function (professions, skills, createHandler) {
            var panel = new Component('div');
            panel.SetAttributes('class', 'panel panel-primary');
            var panel_head = new Component('div');
            panel_head.SetAttributes('class', 'panel-heading');
            panel_head.SetContent('Create Advert');
            var form = new Component('form');
            form.SetAttributes('class', 'form-horizontal');
            var form_group_title = new Component('div');
            form_group_title.SetAttributes('class', 'form-group');
            var form_group_price = new Component('div');
            form_group_price.SetAttributes('class', 'form-group');
            var form_group_professions = new Component('div');
            form_group_professions.SetAttributes('class', 'form-group');
            var form_group_skills = new Component('div');
            form_group_skills.SetAttributes('class', 'form-group');
            var title_block = new Component('div');
            title_block.SetAttributes('class', 'col-xs-12');
            var title = new Component('input');
            title.SetAttributes('class', 'form-control');
            title.SetAttributes('Placeholder', 'Title');
            title_block.Append(title);
            var price_block = new Component('div');
            price_block.SetAttributes('class', 'col-xs-12');
            var price = new Component('input');
            price.SetAttributes('class', 'form-control');
            price.SetAttributes('Placeholder', 'Salary');
            price_block.Append(price);
            var profession_select = new Component('select');
            profession_select.SetAttributes('class', 'form-control');
            var default_profession_option = new Component('option');
            default_profession_option.element.disabled = true;
            default_profession_option.element.selected = true;
            default_profession_option.SetContent('Select profession');
            profession_select.Append(default_profession_option);
            professions.ForEach(function (profession) {
                var option = new Component('option');
                option.SetContent(profession.value);
                option.SetAttributes('value', profession.id.toString());
                profession_select.Append(option);
            });
            var selected_professions_collection = new Collection();
            var selected_professions = new Component('div');
            selected_professions.SetAttributes('class', 'form-control');
            selected_professions.SetContent('Professions: ');
            profession_select.On('change', function (self) {
                var select = self.element;
                if (!selected_professions_collection.Has(parseInt(select.options[select.selectedIndex].value))) {
                    var text = new Component('span');
                    text.SetContent(select.options[select.selectedIndex].innerHTML);
                    var icon = new Component('span');
                    icon.SetAttributes('class', 'glyphicon glyphicon-remove');
                    (function (text, icon, id) {
                        icon.On('click', function () {
                            selected_professions_collection.Remove(id);
                            selected_professions.element.removeChild(text.element);
                            selected_professions.element.removeChild(icon.element);
                            console.log('delete', selected_professions_collection.ToArray());
                        });
                    })(text, icon, parseInt(select.options[select.selectedIndex].value));
                    selected_professions.Append(text);
                    selected_professions.Append(icon);
                    selected_professions_collection.Add(parseInt(select.options[select.selectedIndex].value));
                }
            });
            var skill_select = new Component('select');
            skill_select.SetAttributes('class', 'form-control');
            var default_skill_option = new Component('option');
            default_skill_option.element.disabled = true;
            default_skill_option.element.selected = true;
            default_skill_option.SetContent('Select skills');
            skill_select.Append(default_skill_option);
            skills.ForEach(function (skill) {
                var option = new Component('option');
                option.SetContent(skill.value);
                option.SetAttributes('value', skill.id.toString());
                skill_select.Append(option);
            });
            var selected_skills_collection = new Collection();
            var selected_skills = new Component('div');
            selected_skills.SetContent('Skills: ');
            skill_select.On('change', function (self) {
                var select = self.element;
                if (!selected_skills_collection.Has(parseInt(select.options[select.selectedIndex].value))) {
                    var text = new Component('span');
                    text.SetContent(select.options[select.selectedIndex].innerHTML);
                    var icon = new Component('span');
                    icon.SetAttributes('class', 'glyphicon glyphicon-remove');
                    (function (text, icon, id) {
                        icon.On('click', function () {
                            selected_skills_collection.Remove(id);
                            selected_skills.element.removeChild(text.element);
                            selected_skills.element.removeChild(icon.element);
                            console.log('delete', selected_skills_collection.ToArray());
                        });
                    })(text, icon, parseInt(select.options[select.selectedIndex].value));
                    selected_skills.Append(text);
                    selected_skills.Append(icon);
                    selected_skills_collection.Add(parseInt(select.options[select.selectedIndex].value));
                }
            });
            form_group_title.Append(title_block);
            form_group_price.Append(price_block);
            form_group_professions.Append(selected_professions);
            form_group_professions.Append(profession_select);
            form_group_skills.Append(selected_skills);
            form_group_skills.Append(skill_select);
            var create_button = new Component('button');
            create_button.SetContent('Create advert');
            create_button.SetAttributes('class', 'btn btn-success');
            create_button.On('click', function () {
                createHandler({
                    title: title.element.value,
                    price: parseInt(price.element.value),
                    professions: selected_professions_collection.ToArray(),
                    skills: selected_skills_collection.ToArray()
                });
            });
            form.Append(form_group_title);
            form.Append(form_group_price);
            form.Append(form_group_professions);
            form.Append(form_group_skills);
            var panel_body = new Component('div');
            panel_body.SetAttributes('class', 'panel-body');
            panel_body.Append(form);
            panel.Append(panel_head);
            panel.Append(panel_body);
            this.layout.Append(panel);
            this.layout.Append(create_button);
        };
        AdvertCreateView.prototype.CreatedNotification = function () {
            Notification.GetInstance().Notify('success', 'Created', 1000);
        };
        return AdvertCreateView;
    })();
    Employer.AdvertCreateView = AdvertCreateView;
})(Employer || (Employer = {}));
var Employer;
(function (Employer) {
    var ProfessionMapper = (function () {
        function ProfessionMapper() {
        }
        ProfessionMapper.prototype.Load = function (handler) {
            var _this = this;
            new HttpClient().Execute({
                url: 'data/professions.json',
                method: 'GET',
                callback: function (data) {
                    handler(_this.BuildCollection(data));
                }
            });
        };
        ProfessionMapper.prototype.BuildObject = function (jsonData) {
            if (jsonData) {
                var data = jsonData.data;
                return new Employer.Profession({
                    id: data.id,
                    value: data.value
                });
            }
            return null;
        };
        ProfessionMapper.prototype.BuildCollection = function (jsonData) {
            var collection;
            if (jsonData) {
                collection = new Collection();
                var array = jsonData.collection;
                for (var key in jsonData.collection) {
                    collection.Add(this.BuildObject(array[key]));
                }
            }
            return collection;
        };
        return ProfessionMapper;
    })();
    Employer.ProfessionMapper = ProfessionMapper;
})(Employer || (Employer = {}));
var Employer;
(function (Employer) {
    var SkillMapper = (function () {
        function SkillMapper() {
        }
        SkillMapper.prototype.Load = function (handler) {
            var _this = this;
            new HttpClient().Execute({
                url: 'data/skills.json',
                method: 'GET',
                callback: function (data) {
                    handler(_this.BuildCollection(data));
                }
            });
        };
        SkillMapper.prototype.BuildObject = function (jsonData) {
            if (jsonData) {
                var data = jsonData.data;
                return new Employer.Skill({
                    id: data.id,
                    value: data.value
                });
            }
            return null;
        };
        SkillMapper.prototype.BuildCollection = function (jsonData) {
            var collection;
            if (jsonData) {
                collection = new Collection();
                var array = jsonData.collection;
                for (var key in jsonData.collection) {
                    collection.Add(this.BuildObject(array[key]));
                }
            }
            return collection;
        };
        return SkillMapper;
    })();
    Employer.SkillMapper = SkillMapper;
})(Employer || (Employer = {}));
var Employer;
(function (Employer) {
    var AdvertCreateService = (function () {
        function AdvertCreateService() {
        }
        AdvertCreateService.prototype.CreateAdvert = function (data, createdHandler) {
            console.log("AdvertData", data);
            createdHandler();
        };
        AdvertCreateService.prototype.LoadProfessions = function (handler) {
            new Employer.ProfessionMapper().Load(function (professions) {
                handler(professions);
            });
        };
        AdvertCreateService.prototype.LoadSkills = function (handler) {
            new Employer.SkillMapper().Load(function (skills) {
                handler(skills);
            });
        };
        return AdvertCreateService;
    })();
    Employer.AdvertCreateService = AdvertCreateService;
})(Employer || (Employer = {}));
var Employer;
(function (Employer) {
    var AdvertCreatePresenter = (function () {
        function AdvertCreatePresenter(view, service) {
            this.view = view;
            this.service = service;
        }
        AdvertCreatePresenter.prototype.Start = function () {
            var _this = this;
            this.service.LoadProfessions(function (professions) {
                _this.service.LoadSkills(function (skills) {
                    _this.view.Draw(professions, skills, function (data) {
                        _this.CreateAdvert(data);
                    });
                });
            });
        };
        AdvertCreatePresenter.prototype.CreateAdvert = function (advertData) {
            var _this = this;
            this.service.CreateAdvert(advertData, function () {
                _this.view.CreatedNotification();
            });
        };
        return AdvertCreatePresenter;
    })();
    Employer.AdvertCreatePresenter = AdvertCreatePresenter;
})(Employer || (Employer = {}));
var Employer;
(function (Employer) {
    var EmployerFacade = (function () {
        function EmployerFacade() {
        }
        EmployerFacade.prototype.ShowAdverts = function (layout) {
            var view = new Employer.AdvertsView(layout);
            var service = new Employer.AdvertsService();
            new Employer.AdvertsPresenter(view, service).ShowAdvertsAction();
        };
        EmployerFacade.prototype.ShowAdvertInfo = function (layout, id) {
            var view = new Employer.AdvertInfoView(layout);
            var service = new Employer.AdvertInfoService(id);
            new Employer.AdvertInfoPresenter(view, service).LoadAction();
        };
        EmployerFacade.prototype.CreateAdvert = function (layout) {
            var view = new Employer.AdvertCreateView(layout);
            var service = new Employer.AdvertCreateService();
            var presenter = new Employer.AdvertCreatePresenter(view, service);
            presenter.Start();
        };
        return EmployerFacade;
    })();
    Employer.EmployerFacade = EmployerFacade;
})(Employer || (Employer = {}));
