///<reference path="components/Component.ts"/>
var Notification = (function () {
    function Notification() {
        this.notification = new Component('div');
        if (Notification.instance) {
            throw new Error('Notification');
        }
    }
    Notification.GetInstance = function () {
        return Notification.instance;
    };
    Notification.prototype.GetNotification = function () {
        return this.notification;
    };
    Notification.prototype.Notify = function (notifyType, message, time) {
        var _this = this;
        switch (notifyType) {
            case "success":
                this.notification.SetAttributes('class', 'alert alert-success');
                break;
        }
        this.notification.SetContent(message);
        console.log(time);
        if (time) {
            setTimeout(function () {
                _this.Close();
            }, time);
        }
        if (!time) {
            var close = new Component('a');
            close.SetAttributes('class', 'close');
            close.SetContent('&times;');
            close.On('click', function () {
                _this.Close();
            });
            this.notification.Append(close);
        }
    };
    Notification.prototype.Close = function () {
        this.notification.Clear();
        this.notification.SetAttributes('class', 'close');
    };
    Notification.instance = new Notification();
    return Notification;
})();
//# sourceMappingURL=Notification.js.map