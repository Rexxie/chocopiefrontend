///<reference path="components/Component.ts"/>
///<reference path="components/Body.ts"/>

class ScriptLoader{
    private script: Component;

    constructor(path: string){
        this.script = new Component('script');
        this.script.SetAttributes('src', path);
    }

    public Load(handler?:() => void): void{
        if(handler) {
            this.script.On('load', () => {
                handler();
            });
        }

        Body.GetInstance().Append(this.script);
    }
}