///<reference path="HttpRequest.ts"/>
var HttpClient = (function () {
    function HttpClient() {
        this.http = new HttpRequest();
    }
    HttpClient.prototype.GetRequest = function (url) {
    };
    HttpClient.prototype.PostRequest = function (url, data) {
    };
    HttpClient.prototype.Execute = function (args) {
        this.http.Request({
            url: args.url,
            method: args.method,
            headers: args.headers,
            handler: args.callback,
            data: args.data
        });
    };
    return HttpClient;
})();
//# sourceMappingURL=HttpClient.js.map