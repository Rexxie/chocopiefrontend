///<reference path="components/Component.ts"/>
///<reference path="components/Body.ts"/>
var ScriptLoader = (function () {
    function ScriptLoader(path) {
        this.script = new Component('script');
        this.script.SetAttributes('src', path);
    }
    ScriptLoader.prototype.Load = function (handler) {
        if (handler) {
            this.script.On('load', function () {
                handler();
            });
        }
        Body.GetInstance().Append(this.script);
    };
    return ScriptLoader;
})();
//# sourceMappingURL=ScriptLoader.js.map