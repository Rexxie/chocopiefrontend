///<reference path="HttpRequest.ts"/>

class HttpClient{
    private http: HttpRequest = new HttpRequest();

    public GetRequest(url: string): void{

    }
    public PostRequest(url: string, data?: any): void{

    }
    public Execute(args: { url: string; method: string; data?: any, headers?: { [key: string]: string }, callback?: (resolve) => void }){
        this.http.Request({
            url: args.url,
            method: args.method,
            headers: args.headers,
            handler: args.callback,
            data: args.data
        })
    }
}