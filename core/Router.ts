///<reference path="Collection.ts"/>

class Router {
    private static instance:Router = new Router();

    public static GetInstance():Router {
        return Router.instance;
    }

    constructor() {
        if (Router.instance) {
            throw new Error('Router is singleton');
        }
    }

    //private modules = Array<{ module: string; method: string; callback:(args?) => void }>();
    private modules = new Collection<{ module: string; method: string; callback:(args?) => void }>();

    public Go(module:string, method:string, args?):void {
        this.modules.ForEach((item) => {
            if (item.module == module) {
                if (item.method == method) {
                    if(args){
                        return item.callback(args);
                    }

                    if(!args) {
                        return item.callback();
                    }
                }
            }
        });

        //for (var key in modules) {
        //    if (modules[key].module == module) {
        //        if (modules[key].method == method) {
        //            return modules[key].callback();
        //        }
        //    }
        //}
    }

    public SetModule(module:string, method:string, callback:(args?) => void):void {
        if (module != null && method != null && callback != null) {
            this.modules.Add({
                module: module,
                method: method,
                callback: callback
            })
        }
    }
}
