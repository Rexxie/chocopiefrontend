///<reference path="components/Component.ts"/>

class Notification{
    private static instance: Notification = new Notification();

    public static GetInstance(): Notification{
        return Notification.instance;
    }
    private notification: Component = new Component('div');

    constructor(){
        if(Notification.instance){
            throw new Error('Notification');
        }
    }
    public GetNotification(): Component{
        return this.notification;
    }

    public Notify(notifyType: string, message: string, time?: number ): void{

        switch (notifyType) {
            case "success" :
                this.notification.SetAttributes('class', 'alert alert-success');
                break;
        }

        this.notification.SetContent(message);

        console.log(time);
        if(time) {
            setTimeout(() => {
                this.Close();
            }, time);
        }

        if(!time) {
            var close = new Component('a');
            close.SetAttributes('class', 'close');
            close.SetContent('&times;');
            close.On('click', () => {
                this.Close();
            });
            this.notification.Append(close);
        }

    }
    private Close(): void{
        this.notification.Clear();
        this.notification.SetAttributes('class', 'close');
    }
}