var Component = (function () {
    function Component(tag) {
        this.element = document.createElement(tag);
        this._events = Array();
    }
    Component.prototype.Append = function (component) {
        this.element.appendChild(component.element);
    };
    Component.prototype.SetAttributes = function (name, value) {
        this.element.setAttribute(name, value);
    };
    Component.prototype.SetContent = function (content) {
        this.element.innerHTML = content;
    };
    Component.prototype.Clear = function () {
        this.element.innerHTML = '';
    };
    Component.prototype.On = function (eventName, callback) {
        var _this = this;
        for (var key in this._events) {
            if (this._events[key].name == eventName) {
                this.element.addEventListener(eventName, function (e) {
                    callback(_this);
                }, false);
                return;
            }
        }
        var event = document.createEvent('Event');
        event.initEvent(eventName, true, true);
        this.element.addEventListener(eventName, function (e) {
            e.stopPropagation();
            callback(_this);
        }, false);
        this._events.push({ name: eventName, event: event });
    };
    Component.prototype.Trigger = function (eventName) {
        for (var key in this._events) {
            if (eventName == this._events[key].name) {
                this.element.dispatchEvent(this._events[key].event);
                return;
            }
        }
        //this.element.dispatchEvent(this._events.get(eventName));
    };
    return Component;
})();
//# sourceMappingURL=Component.js.map