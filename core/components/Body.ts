///<reference path="Component.ts"/>

class Body{
    private static instance: Body = new Body();

    public static GetInstance(): Body{
        return Body.instance;
    }
    constructor(){
        if(Body.instance){
            throw new Error('Body');
        }
    }
    private body = document.body;

    public Append(component: Component): void {
        this.body.appendChild(component.element)
    }
    public GetBody(): HTMLElement{
        return this.body;
    }
}