class Component {
    private _events:Array<{ name: string; event: Event }>;
    public element:HTMLElement;

    constructor(tag:string) {
        this.element = document.createElement(tag);
        this._events = Array();
    }

    public Append(component:Component):void {
        this.element.appendChild(component.element);
    }

    public SetAttributes(name:string, value:string):void {
        this.element.setAttribute(name, value);
    }

    public SetContent(content:string):void {
        this.element.innerHTML = content;
    }
    public Clear(): void{
        this.element.innerHTML = '';
    }
    public On(eventName:string, callback:(component?) => void):void {
        for (var key in this._events) {
            if (this._events[key].name == eventName) {
                this.element.addEventListener(eventName, (e) => {
                    callback(this);
                }, false);
                return;
            }
        }
        var event = document.createEvent('Event');
        event.initEvent(eventName, true, true);

        this.element.addEventListener(eventName, (e) => {
            e.stopPropagation();
            callback(this);
        }, false);
        this._events.push({ name: eventName, event: event });
    }

    public Trigger(eventName:string):void {
        for (var key in this._events) {
            if (eventName == this._events[key].name) {
                this.element.dispatchEvent(this._events[key].event);
                return
            }
        }
        //this.element.dispatchEvent(this._events.get(eventName));
    }
}
