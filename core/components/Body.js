///<reference path="Component.ts"/>
var Body = (function () {
    function Body() {
        this.body = document.body;
        if (Body.instance) {
            throw new Error('Body');
        }
    }
    Body.GetInstance = function () {
        return Body.instance;
    };
    Body.prototype.Append = function (component) {
        this.body.appendChild(component.element);
    };
    Body.prototype.GetBody = function () {
        return this.body;
    };
    Body.instance = new Body();
    return Body;
})();
//# sourceMappingURL=Body.js.map