class HttpRequest{
    private httpRequest: XMLHttpRequest = new XMLHttpRequest();

    public Request(args: { method: string; url: string; headers?: {[key: string]: string}; data?: string; handler?: (response?: string) => void; errorHandler?: (msg: string) => void}): void{
        this.httpRequest = new XMLHttpRequest();

        this.httpRequest.open(args.method, args.url, true);

        for(var key in args.headers){
            this.httpRequest.setRequestHeader(key, args.headers[key]);
        }

        /*TODO throw it to Mappers settings*/
        this.httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

        this.httpRequest.onloadend = () => {
            if(this.httpRequest.status == 200){
                return args.handler( JSON.parse(this.httpRequest.responseText));
            }

            if(this.httpRequest.status == 204){
                return args.handler();
            }
        };

        if(args.errorHandler){
            this.httpRequest.onerror = () => {
                args.errorHandler(this.httpRequest.statusText);
            }
        }

        this.httpRequest.send(args.data);
    }

    public OnError(handler: (msg: string) => void): void{
        this.httpRequest.addEventListener('error', () => {
            handler(this.httpRequest.statusText);
        });
    }
}
