var HttpRequest = (function () {
    function HttpRequest() {
        this.httpRequest = new XMLHttpRequest();
    }
    HttpRequest.prototype.Request = function (args) {
        var _this = this;
        this.httpRequest = new XMLHttpRequest();
        this.httpRequest.open(args.method, args.url, true);
        for (var key in args.headers) {
            this.httpRequest.setRequestHeader(key, args.headers[key]);
        }
        /*TODO throw it to Mappers settings*/
        this.httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        this.httpRequest.onloadend = function () {
            if (_this.httpRequest.status == 200) {
                return args.handler(JSON.parse(_this.httpRequest.responseText));
            }
            if (_this.httpRequest.status == 204) {
                return args.handler();
            }
        };
        if (args.errorHandler) {
            this.httpRequest.onerror = function () {
                args.errorHandler(_this.httpRequest.statusText);
            };
        }
        this.httpRequest.send(args.data);
    };
    HttpRequest.prototype.OnError = function (handler) {
        var _this = this;
        this.httpRequest.addEventListener('error', function () {
            handler(_this.httpRequest.statusText);
        });
    };
    return HttpRequest;
})();
//# sourceMappingURL=HttpRequest.js.map