var Collection = (function () {
    function Collection() {
        this.array = [];
    }
    Collection.prototype.Add = function (item) {
        this.array.push(item);
    };
    Collection.prototype.Remove = function (item) {
        for (var key in this.array) {
            if (this.array[key] == item) {
                this.array.splice(key, 1);
                return;
            }
        }
    };
    Collection.prototype.Has = function (value) {
        for (var _i = 0, _a = this.array; _i < _a.length; _i++) {
            var item = _a[_i];
            if (item == value) {
                return true;
            }
        }
        return false;
    };
    Collection.prototype.ForEach = function (handler) {
        for (var _i = 0, _a = this.array; _i < _a.length; _i++) {
            var item = _a[_i];
            handler(item);
        }
    };
    Collection.prototype.ToArray = function () {
        return this.array;
    };
    return Collection;
})();
//# sourceMappingURL=Collection.js.map