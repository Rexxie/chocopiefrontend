class Collection<T>{
    private array: Array<T> = [];

    public Add(item: T): void{
        this.array.push(item);
    }
    public Remove(item: T): void{
        for(var key in this.array) {
            if(this.array[key] == item){
                this.array.splice(key, 1);
                return;
            }
        }
    }
    public Has(value: T): boolean{
        for(var item of this.array) {
            if(item == value) {
                return true;
            }
        }
        return false;
    }

    public ForEach(handler: (item: T) => void): void{
        for(var item of this.array) {
            handler(item);
        }
    }
    public ToArray(): Array<T>{
        return this.array;
    }
}