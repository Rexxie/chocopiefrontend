///<reference path="Collection.ts"/>
var Router = (function () {
    function Router() {
        //private modules = Array<{ module: string; method: string; callback:(args?) => void }>();
        this.modules = new Collection();
        if (Router.instance) {
            throw new Error('Router is singleton');
        }
    }
    Router.GetInstance = function () {
        return Router.instance;
    };
    Router.prototype.Go = function (module, method, args) {
        this.modules.ForEach(function (item) {
            if (item.module == module) {
                if (item.method == method) {
                    if (args) {
                        return item.callback(args);
                    }
                    if (!args) {
                        return item.callback();
                    }
                }
            }
        });
        //for (var key in modules) {
        //    if (modules[key].module == module) {
        //        if (modules[key].method == method) {
        //            return modules[key].callback();
        //        }
        //    }
        //}
    };
    Router.prototype.SetModule = function (module, method, callback) {
        if (module != null && method != null && callback != null) {
            this.modules.Add({
                module: module,
                method: method,
                callback: callback
            });
        }
    };
    Router.instance = new Router();
    return Router;
})();
//# sourceMappingURL=Router.js.map