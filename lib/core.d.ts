declare class Collection<T> {
    private array;
    Add(item: T): void;
    Remove(item: T): void;
    Has(value: T): boolean;
    ForEach(handler: (item: T) => void): void;
    ToArray(): Array<T>;
}
declare class Router {
    private static instance;
    static GetInstance(): Router;
    constructor();
    private modules;
    Go(module: string, method: string, args?: any): void;
    SetModule(module: string, method: string, callback: (args?) => void): void;
}
declare class Component {
    private _events;
    element: HTMLElement;
    constructor(tag: string);
    Append(component: Component): void;
    SetAttributes(name: string, value: string): void;
    SetContent(content: string): void;
    Clear(): void;
    On(eventName: string, callback: (component?) => void): void;
    Trigger(eventName: string): void;
}
declare class Body {
    private static instance;
    static GetInstance(): Body;
    constructor();
    private body;
    Append(component: Component): void;
    GetBody(): HTMLElement;
}
declare class HttpRequest {
    private httpRequest;
    Request(args: {
        method: string;
        url: string;
        headers?: {
            [key: string]: string;
        };
        data?: string;
        handler?: (response?: string) => void;
        errorHandler?: (msg: string) => void;
    }): void;
    OnError(handler: (msg: string) => void): void;
}
declare class HttpClient {
    private http;
    GetRequest(url: string): void;
    PostRequest(url: string, data?: any): void;
    Execute(args: {
        url: string;
        method: string;
        data?: any;
        headers?: {
            [key: string]: string;
        };
        callback?: (resolve) => void;
    }): void;
}
declare class ScriptLoader {
    private script;
    constructor(path: string);
    Load(handler?: () => void): void;
}
declare class Notification {
    private static instance;
    static GetInstance(): Notification;
    private notification;
    constructor();
    GetNotification(): Component;
    Notify(notifyType: string, message: string, time?: number): void;
    private Close();
}
